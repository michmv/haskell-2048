# Terminal game 2048

Clone of the game 2048, written for fun with use language Haskell.

![terminal-gif](./image/preview.gif)

# Use
```sh
cd ...
$ stack build
$ stack exec game2048
```

For work need terminal with support 256 color.
