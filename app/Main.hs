module Main where

import Data.Time (getCurrentTime, UTCTime)
import Basegame (run, ActionBind(..))
import System.Environment as E (getArgs)

import Game2048
import Types
import UI
import Functions

main :: IO ()
main = do
    args <- getArgs
    t <- getCurrentTime
    run 10 1000 (initNgame t args) drawUI initAttrMap

initNgame :: UTCTime -> [String] -> NGame
initNgame t args = NGame
    { nActions         = initActions
    , nActionsBind     = [
          ActionBind "quit"  ["Esc","q"]
        , ActionBind "left"  ["Left"]
        , ActionBind "right" ["Right"]
        , ActionBind "up"    ["Up"]
        , ActionBind "down"  ["Down"]
        , ActionBind "new"   ["n"]
        , ActionBind "help"  ["h"]
        ]
    , nActionOtherwise = Nothing
    , nStop            = False
    , nProcessesIO     = initProcessesIO
    , nProcesses       = initProcesses
    , nWindow          = (0,0)
    --
    , nCTime           = t
    , nField           = initField
    , nHelp            = False
    , nFinish          = False
    , nAnimation       = []
    , nLang            = setLangeage args
    }

setLangeage :: [String] -> Lang
setLangeage args = let f a b = b || a == "--ru" in
    if foldr f False args then RU else EN
