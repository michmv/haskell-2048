import Test.Hspec
import Game2048

listCommands = [
      Command (P 3 0) (P 0 0) 2
    , Command (P 2 1) (P 0 1) 4
    , Command (P 3 2) (P 0 2) 8
    , Command (P 2 3) (P 0 3) 16
    ]

field = Field [
      [2,4,8,16]
    , [0,0,0,0 ]
    , [0,0,0,0 ]
    , [0,0,0,0 ]
    ]

field2 = Field [
      [0,0,0,0]
    , [0,2,0,0]
    , [0,0,4,0]
    , [0,0,0,0]
    ]

field_bugfix_1 = Field [
      [0,0,2,0]
    , [0,0,0,0]
    , [2,4,0,0]
    , [2,8,0,0]
    ]

main :: IO ()
main = hspec $ do

    describe "moveLeftRow" $ do

        it "[0,0,0,0]" $ moveLeftRow [0,0,0,0] 0 `shouldBe` []
        it "[0,0,0,2]" $ moveLeftRow [0,0,0,2] 0 `shouldBe` [Command (P 3 0) (P 0 0) 2]
        it "[0,0,2,0]" $ moveLeftRow [0,0,2,0] 0 `shouldBe` [Command (P 2 0) (P 0 0) 2]
        it "[0,0,4,2]" $ moveLeftRow [0,0,4,2] 0 `shouldBe` [Command (P 2 0) (P 0 0) 4, Command (P 3 0) (P 1 0) 2]
        it "[0,4,0,2]" $ moveLeftRow [0,4,0,2] 0 `shouldBe` [Command (P 1 0) (P 0 0) 4, Command (P 3 0) (P 1 0) 2]
        it "[0,4,2,0]" $ moveLeftRow [0,4,2,0] 0 `shouldBe` [Command (P 1 0) (P 0 0) 4, Command (P 2 0) (P 1 0) 2]
        it "[0,8,4,2]" $ moveLeftRow [0,8,4,2] 0 `shouldBe` [Command (P 1 0) (P 0 0) 8, Command (P 2 0) (P 1 0) 4, Command (P 3 0) (P 2 0) 2]

        it "[4,8,0,0]" $ moveLeftRow [4,8,0,0] 0 `shouldBe` []

    describe "moveLeftRowMerge" $ do

        it "[0,0,0,0]" $ moveLeftRow [0,0,0,0] 0 `shouldBe` []
        it "[0,0,0,2]" $ moveLeftRow [0,0,0,2] 0 `shouldBe` [Command (P 3 0) (P 0 0) 2]
        it "[0,0,2,0]" $ moveLeftRow [0,0,2,0] 0 `shouldBe` [Command (P 2 0) (P 0 0) 2]
        it "[0,0,2,2]" $ moveLeftRow [0,0,2,2] 0 `shouldBe` [Command (P 2 0) (P 0 0) 2, Command (P 3 0) (P 0 0) 2]
        it "[0,2,0,2]" $ moveLeftRow [0,2,0,2] 0 `shouldBe` [Command (P 1 0) (P 0 0) 2, Command (P 3 0) (P 0 0) 2]
        it "[0,2,2,0]" $ moveLeftRow [0,2,2,0] 0 `shouldBe` [Command (P 1 0) (P 0 0) 2, Command (P 2 0) (P 0 0) 2]
        it "[0,2,2,2]" $ moveLeftRow [0,2,2,2] 0 `shouldBe` [Command (P 1 0) (P 0 0) 2, Command (P 2 0) (P 0 0) 2, Command (P 3 0) (P 1 0) 2]

    describe "moveLeft" $ do

        it "moveLeft Field" $ moveLeft (Field [[0,0,0,2], [0,0,2,0], [0,2,0,0], [2,0,0,0]])
            `shouldBe` [
                  Command (P 3 0) (P 0 0) 2
                , Command (P 2 1) (P 0 1) 2
                , Command (P 1 2) (P 0 2) 2
                ]

    describe "rotateCommands" $ do
        it "rotateCommands Right" $ rotateCommands listCommands RRight
            `shouldBe` [
                  Command (P 3 3) (P 3 0) 2
                , Command (P 2 2) (P 2 0) 4
                , Command (P 1 3) (P 1 0) 8
                , Command (P 0 2) (P 0 0) 16
                ]

        it "rotateCommands Left" $ rotateCommands listCommands RLeft
            `shouldBe` [
                  Command (P 0 0) (P 0 3) 2
                , Command (P 1 1) (P 1 3) 4
                , Command (P 2 0) (P 2 3) 8
                , Command (P 3 1) (P 3 3) 16
                ]

    describe "rotateField" $ do
        it "rotateField Right" $ rotateField field RRight
            `shouldBe` Field [
                  [0,0,0,2]
                , [0,0,0,4]
                , [0,0,0,8]
                , [0,0,0,16]
                ]

        it "rotateField Left" $ rotateField field RLeft
            `shouldBe` Field [
                  [16,0,0,0]
                , [8 ,0,0,0]
                , [4 ,0,0,0]
                , [2 ,0,0,0]
                ]

    describe "readMaximumScore" $ do
        it "readMaximumScore" $ readMaximumScore field `shouldBe` 16

    describe "checkFinishGame" $ do
        it "checkZero" $ checkFinishGame (Field [[0,1,2,3], [4,5,6,7]])
            `shouldBe` False

        it "findPair horizontal" $ checkFinishGame (Field [[9,1,2,3], [4,5,6,6]])
            `shouldBe` False

        it "findPair vertical" $ checkFinishGame (Field [[9,1,2,7], [4,5,6,7]])
            `shouldBe` False

        it "checkFinishGame" $ checkFinishGame (Field [[1,2,3,4], [5,6,7,8]])
            `shouldBe` True

    describe "createCommands" $ do
        it "Up" $ createCommands field2 MUp `shouldBe` [Command (P 2 2) (P 2 0) 4, Command (P 1 1) (P 1 0) 2]

        it "Right" $ createCommands field2 MRight `shouldBe` [Command (P 2 2) (P 3 2) 4, Command (P 1 1) (P 3 1) 2]

        it "Down" $ createCommands field2 MDown `shouldBe` [Command (P 1 1) (P 1 3) 2, Command (P 2 2) (P 2 3) 4]

        it "Left" $ createCommands field2 MLeft `shouldBe` [Command (P 1 1) (P 0 1) 2, Command (P 2 2) (P 0 2) 4]

    describe "applyCommands" $ do
        it "Up" $ applyCommands (createCommands field2 MUp) field2 `shouldBe` (Field 
                                                                                [ [0,2,4,0]
                                                                                , [0,0,0,0]
                                                                                , [0,0,0,0]
                                                                                , [0,0,0,0]
                                                                                ])

        it "Right" $ applyCommands (createCommands field2 MRight) field2 `shouldBe` (Field 
                                                                                [ [0,0,0,0]
                                                                                , [0,0,0,2]
                                                                                , [0,0,0,4]
                                                                                , [0,0,0,0]
                                                                                ])

        it "Down" $ applyCommands (createCommands field2 MDown) field2 `shouldBe` (Field 
                                                                                [ [0,0,0,0]
                                                                                , [0,0,0,0]
                                                                                , [0,0,0,0]
                                                                                , [0,2,4,0]
                                                                                ])

        it "Left" $ applyCommands (createCommands field2 MLeft) field2 `shouldBe` (Field 
                                                                                [ [0,0,0,0]
                                                                                , [2,0,0,0]
                                                                                , [4,0,0,0]
                                                                                , [0,0,0,0]
                                                                                ])

    describe "bug fix #1" $ do
        it "Down" $ applyCommands (createCommands field_bugfix_1 MDown) field_bugfix_1 `shouldBe` (Field 
                                                                                [ [0,0,0,0]
                                                                                , [0,0,0,0]
                                                                                , [0,4,0,0]
                                                                                , [4,8,2,0]
                                                                                ])
