{-# LANGUAGE MultiWayIf #-}
module UI where

import Brick
    ( Widget, AttrName, AttrMap
    , attrName, on, withAttr, attrMap
    , vLimit, hLimit, padAll, fill, translateBy, emptyWidget, Location(..)
    , str, (<+>), (<=>)
    )
import Basegame (Name)
import Graphics.Vty as V (defAttr, rgbColor)
import qualified Brick.Widgets.Center as C (center, hCenter)
import qualified Brick.Widgets.Border as B (borderAttr, border)
import Data.Time (UTCTime, diffUTCTime)
import Data.Sequence as DS (fromList, update)
import Data.Foldable (toList)

import Types
import Game2048

------------------------------------------------

drawUI :: NGame -> [Widget Name]
drawUI g = 
    if | width < 40  -> drawMessage [t "narrow1" (nLang g) , t "narrow2" (nLang g)]
       | height < 23 -> drawMessage [t "low1" (nLang g)    , t "low2" (nLang g)]
       | otherwise   ->
            ( if nHelp g then [ translateBy (Location (width `div` 2 - 20, height `div` 2 - 11)) $ drawHelp (nLang g) ]
              else [] ) ++
            -- ^ show help

            ( if nFinish g then [ translateBy (Location (width `div` 2 - 14, height `div` 2 - 4)) (drawFinish g) ] 
              else [] ) ++
            -- ^ show finish window

            animationNewCell (nAnimation g) (width, height) (nCTime g) ++
            -- ^ анимация появления новой ячейки

            drawField (animationMoveCell (nAnimation g) (recalField field 0) (nCTime g))
                (width `div` 2 - 18, height `div` 2 - 10) ++
            -- ^ отрисовка ячеек и их анимация

            [ translateBy (Location (width `div` 2 - 20, height `div` 2 - 11)) $
                B.border (hLimit 38 (vLimit 20 (withAttr colorBackgroun $ fill ' ')))
              -- ^ рамка вокруг поля

            , translateBy (Location (width `div` 2 - 20, height `div` 2 + 11)) $
                withAttr colorBackgroun $ vLimit 1 $ hLimit 40 $ C.center $ str $ t "help" (nLang g)
              -- ^ текст с подсказкой

            , withAttr colorBackgroun $ fill ' '
              -- ^ цвет фона
            ]
    where
        (Field field) = nField g
        (width, height) = nWindow g

-- | Анимация перемещения ячеек
animationMoveCell :: [Animation] -> [(Int, Int, Int)] -> UTCTime -> [(Int, Int, Int)]
animationMoveCell [] field _ = field
animationMoveCell (i:is) field t = case i of
    Move{} -> animationMoveCell is (applyMove' i field) t
    _ -> animationMoveCell is field t
    where
        applyMove' :: Animation -> [(Int, Int, Int)] -> [(Int, Int, Int)]
        applyMove' a field =
            let (Move time duration commands _) = a
                diff = realToFrac $ diffUTCTime t time in
            move' commands field diff duration
        
        move' :: [Command] -> [(Int, Int, Int)] -> Double -> Double -> [(Int, Int, Int)]
        move' [] field _ _ = field
        move' (c:cs) field diff duration =
            let cell = (max xn 27, max yn 15, v)
                index = x1 + (y1 * 4)
                (Command (P x1 y1) (P x2 y2) _) = c
                offsetX = fromIntegral $ (x2-x1)*9 :: Double
                offsetY = fromIntegral $ (y2-y1)*5 :: Double
                xn = ceiling (diff * vx) + x -- Int
                yn = ceiling (diff * vy) + y -- Int
                vx = offsetX / duration -- Double
                vy = offsetY / duration -- Double
                (x,y,v) = get' index field
                max x m = if x > m then m else x
            in
            move' cs (set' index field cell) diff duration

        set' :: Int -> [(Int, Int, Int)] -> (Int, Int, Int) -> [(Int, Int, Int)]
        set' index field cell = toList $ DS.update index cell (fromList field)

        get' :: Int -> [(Int, Int, Int)] -> (Int, Int, Int)
        get' index field = field !! index

-- | Анимация появления нового числа
animationNewCell :: [Animation] -> (Int, Int) -> UTCTime -> [Widget Name]
animationNewCell [] _ _ = [emptyWidget]
animationNewCell (i:is) (w,h) t = case i of
    (New time finish value (x,y) _) -> animationNewCellWidget (diff time) finish value x y w h ++
        animationNewCell is (w,h) t
    _ -> animationNewCell is (w,h) t
    where
        diff time_ = realToFrac $ diffUTCTime t time_

-- | Создание виджета показывающего появление нового числа
animationNewCellWidget :: Double -> Double -> Int -> Int -> Int -> Int -> Int -> [Widget Name]
animationNewCellWidget diff finish value xN yN w h =
    [ if | step > 3  -> translateBy (Location (x+1,y+0)) (frame4 value)
         | step > 2  -> translateBy (Location (x+2,y+0)) (frame3 value)
         | step > 1  -> translateBy (Location (x+3,y+1)) (frame2 value)
         | otherwise -> translateBy (Location (x+4,y+2)) (frame1 value)
    , 
    translateBy (Location (x,y)) (drawCell 0) ]
    where
        x = w `div` 2 - 18 + (xN * 9)
        y = h `div` 2 - 10 + (yN * 5)
        step = 4 / finish * diff

frame1 :: Int -> Widget Name
frame1 n = withAttr (cellColor n) $ str " "

frame2 :: Int -> Widget Name
frame2 n = withAttr (cellColor n) $
    str "   " <=>
    str "   " <=>
    str "   "

frame3 :: Int -> Widget Name
frame3 n = withAttr (cellColor n) $
    str "     " <=>
    str "     " <=>
    str "     " <=>
    str "     " <=>
    str "     "

frame4 :: Int -> Widget Name
frame4 n = withAttr (cellColor n) $
    str "       " <=>
    str "       " <=>
    str "       " <=>
    str "       " <=>
    str "       "

-- | Показать сообщение пользователю
drawMessage :: [String] -> [Widget Name]
drawMessage msg = [ withAttr colorBackgroun $ C.center $ drawMessage' msg ]
    where
        drawMessage' [] = emptyWidget
        drawMessage' (x:xs) = message x <=> drawMessage' xs

        message s = vLimit 1 $ C.hCenter $ str s

-- | Нарисовать поле
drawField :: [(Int, Int, Int)] -> (Int, Int) -> [Widget Name]
drawField [] _ = []
drawField (i:is) size =
    let (x, y, n) = i
        (w,h) = size
    in
    if n == 0 then drawField is size
        else
            translateBy (Location (x+w,y+h)) (drawCell n) : drawField is size

-- | Пересчитать блоки, вычислить для них точное смещение и выстроить в одну цепочку
recalField :: [[Int]] -> Int -> [(Int, Int, Int)]
recalField [] _ = []
recalField (x:xs) n = recalFieldRow x n 0 ++ recalField xs (n+1)
    where
        recalFieldRow :: [Int] -> Int -> Int -> [(Int, Int, Int)]
        recalFieldRow [] _ _ = []
        recalFieldRow (n:ns) y x = (x*9, y*5, n) : recalFieldRow ns y (x+1)

drawCell :: Int -> Widget Name
drawCell n = 
    withAttr (cellColor n) $
    str  "         " <=>
    str  "         " <=>
    str (" "++x++" ") <=>
    str  "         " <=>
    str  "         "
    where
        x = numString n
        numString n = if n == 0 then "       " else centerString (show n)
        centerString s = take 7 (space s ++ s ++ space s ++ "  ")
        space s = multStr ((7 - length s) `div` 2) " "

drawHelp :: Lang -> Widget Name
drawHelp lang = B.border $ withAttr colorBackgroun $ vLimit 20 $ hLimit 38 $ padAll 2 $
    str (t "help_text" lang) <=> fill ' '

drawFinish :: NGame -> Widget Name
drawFinish g = withAttr colorWin $ vLimit 8 $ hLimit 28 $
    C.center $ finishText g

finishText :: NGame -> Widget Name
finishText g =
    let lang = nLang g in
    C.hCenter (str (t "game_over" lang)) <=>
    C.hCenter (str (t "result1" lang)) <=>
    C.hCenter (str $ t "result2" lang ++
        show (readMaximumScore (nField g)) ++
        t "result3" lang) <=>
    C.hCenter (str $ t "pressN" lang)

multStr :: Int -> String -> String
multStr n str = if n <= 0 then "" else str ++ multStr (n-1) str

cellColor :: Int -> AttrName
cellColor n =
    if | n == 0    -> colorTileZero
       | n == 2    -> colorTile2
       | n == 4    -> colorTile4
       | n == 8    -> colorTile8
       | n == 16   -> colorTile16
       | n == 32   -> colorTile32
       | n == 64   -> colorTile64
       | n == 128  -> colorTile128
       | n == 256  -> colorTile256
       | n == 512  -> colorTile512
       | n == 1024 -> colorTile1024
       | n == 2048 -> colorTile2048
       | otherwise -> colorTileMore

initAttrMap :: AttrMap
initAttrMap = attrMap V.defAttr [
      (colorTileZero,  rgbColor 28  28  28  `on` rgbColor 28  28  28)
    , (colorTile2,     rgbColor 28  28  28  `on` rgbColor 0   135 255)
    , (colorTile4,     rgbColor 28  28  28  `on` rgbColor 255 215 95)
    , (colorTile8,     rgbColor 28  28  28  `on` rgbColor 135 215 95)
    , (colorTile16,    rgbColor 28 28 28    `on` rgbColor 215 135  0)
    , (colorTile32,    rgbColor 208 208 208 `on` rgbColor 0   95  0)
    , (colorTile64,    rgbColor 28  28  28  `on` rgbColor 215 95  215)
    , (colorTile128,   rgbColor 208 208 208 `on` rgbColor 95  0   95)
    , (colorTile256,   rgbColor 208 208 208 `on` rgbColor 0   0   135)
    , (colorTile512,   rgbColor 28  28  28  `on` rgbColor 128 128 128)
    , (colorTile1024,  rgbColor 28  28  28  `on` rgbColor 0   255 215)
    , (colorTile2048,  rgbColor 208 208 208 `on` rgbColor 95  0   175)
    , (colorTileMore,  rgbColor 208 208 208 `on` rgbColor 215 0   0)
    , (colorBackgroun, rgbColor 208 208 208 `on` rgbColor 28  28  28)
    , (B.borderAttr,   rgbColor 208 208 208 `on` rgbColor 28  28  28)
    , (colorWin,       rgbColor 208 208 208 `on` rgbColor 0   0   0)
    ]

colorTileZero  = attrName "colorTileZero"
colorTile2     = attrName "colorTile2"
colorTile4     = attrName "colorTile4"
colorTile8     = attrName "colorTile8"
colorTile16    = attrName "colorTile16"
colorTile32    = attrName "colorTile32"
colorTile64    = attrName "colorTile64"
colorTile128   = attrName "colorTile128"
colorTile256   = attrName "colorTile256"
colorTile512   = attrName "colorTile512"
colorTile1024  = attrName "colorTile1024"
colorTile2048  = attrName "colorTile2048"
colorTileMore  = attrName "colorTileMore"
colorBackgroun = attrName "colorBackgroun"
colorWin       = attrName "colorWin"

-- | Translate
t :: String -> Lang -> String
t name RU = searchPair (name++"_ru") listString
t name EN = searchPair (name++"_en") listString

searchPair :: String -> [(String, String)] -> String
searchPair str [] = str
searchPair str ((name, value):xs) = if name == str then value else searchPair str xs

listString :: [(String, String)]
listString = [
      ("narrow1_en",   "The terminal window")
    , ("narrow2_en",   "is too narraw.")
    , ("low1_en",      "The terminal window")
    , ("low2_en",      "too low.")
    , ("help_en",      "Press `h` for see help.")
    , ("help_text_en", helpText_en)
    , ("game_over_en", "Game over.\n")
    , ("result1_en",   "Your result in the game\n")
    , ("result2_en",   "is ")
    , ("result3_en",   " points.\n")
    , ("pressN_en",    "Press `n` for new game.")

    , ("narrow1_ru",   "Окно терминала")
    , ("narrow2_ru",   "слишком узкое.")
    , ("low1_ru",      "Окно терминала")
    , ("low2_ru",      "слишком низкое.")
    , ("help_ru",      "Нажмите `h` что бы получить помощь.")
    , ("help_text_ru", helpText_ru)
    , ("game_over_ru", "Игра окончена.\n")
    , ("result1_ru",   "Ваш результат в игре\n")
    , ("result2_ru",   "составляет ")
    , ("result3_ru",   " очков.\n")
    , ("pressN_ru",    "Заново нажмите `n`.")
    ]

helpText_en :: String
helpText_en =
    "2048 - terminal\n" ++
    " \n" ++
    "--------\n" ++
    " \n" ++
    "This is clone of game 2048," ++
    "written for fun.\n" ++
    " \n" ++
    "Controll:\n" ++
    "Arrow key to move boxs.\n" ++
    "Press `n` for new game.\n" ++
    "Esc or `q` to exit.\n" ++
    " \n" ++
    " \n" ++
    "Press any key to continue."

helpText_ru :: String
helpText_ru =
    "2048 - terminal\n" ++
    " \n" ++
    "--------\n" ++
    " \n" ++
    "Это клон игры 2048,\n" ++
    "написанный для развлечения.\n" ++
    " \n" ++
    "Управление:\n" ++
    "Стрелки курсора для\n" ++
    "перемещения курсора.\n" ++
    "Нажмите `n` для новой игры.\n" ++
    "Esc или `q` что бы выйти.\n" ++
    " \n" ++
    "Нажмите любую кнопку\n" ++
    "для продолжения."
