module Functions where

import Data.Time (UTCTime, getCurrentTime, diffUTCTime)
import Basegame (
      removeProcessIO, ProcessIO(..), Process(..), addProcessIO
    , Action(..), setStop
    )

import Types
import Game2048

-- | Выход из игры
actionQuit :: String -> NGame -> NGame
actionQuit _ = setStop True

-- | Влево
actionLeft :: String -> NGame -> NGame
actionLeft _ g = moveField g MLeft

-- | Вправо
actionRight :: String -> NGame -> NGame
actionRight _ g = moveField g MRight

-- | Вверх
actionUp :: String -> NGame -> NGame
actionUp _ g = moveField g MUp

-- | Вниз
actionDown :: String -> NGame -> NGame
actionDown _ g = moveField g MDown

-- | Новая игра
actionNew :: String -> NGame -> NGame
actionNew _ g = 
    g{ nActions = initActions
     , nActionOtherwise = Nothing
     , nProcessesIO = initProcessesIO
     , nProcesses = initProcesses
     , nField = initField
     , nHelp = False
     , nFinish = False
     , nAnimation = []
     }

-- | Показать помощь
actionHelp :: String -> NGame -> NGame
actionHelp _ g = g{ nActions = [ Action "quit" actionQuit ]
                  , nHelp = True 
                  , nActionOtherwise = Just actionHideHelp
                  }

-- | Убрать окно помощи при клике на любую кнопку
actionHideHelp :: String -> NGame -> NGame
actionHideHelp _ g = g{ nHelp = False
                      , nActionOtherwise = Nothing
                      , nActions = initActions
                      }

-- | Выполнить ход
moveField :: NGame -> Move -> NGame
moveField g action =
    let field = nField g
        commands = createCommands field action
        new = g{nAnimation = nAnimation g ++ [animationMove (nCTime g) commands]}
    in
    if checkAnimation g || null commands then g else new
    -- ^ если не произошло изменение на поле то не создавать новую ячейку

-- | Начало игры
initActions = [
      Action "quit"  actionQuit
    , Action "left"  actionLeft
    , Action "right" actionRight
    , Action "up"    actionUp
    , Action "down"  actionDown
    , Action "new"   actionNew
    , Action "help"  actionHelp
    ]

----------------------------------------------------

-- | Создать новую ячейку на поле
createNumber :: String -> NGame -> IO NGame
createNumber name g = do
    new <- createRandomNumber (nField g)
    let (NewCell x y v) = new
    return $ removeProcessIO name (g{
          nAnimation = nAnimation g ++ [animationNew (nCTime g) v x y]
        })

-- | Создать две новые ячейки на поле в начале игры
createNumberTwo :: String -> NGame -> IO NGame
createNumberTwo name g = do
    new1 <- createRandomNumber (nField g)
    new2 <- createRandomNumber (applyNewCellOnField new1 (nField g))
    let (NewCell x1 y1 v1) = new1
    let (NewCell x2 y2 v2) = new2
    return $ removeProcessIO name (g{
          nAnimation = nAnimation g ++
            [animationNew (nCTime g) v1 x1 y1] ++
            [animationNew (nCTime g) v2 x2 y2]
        })

-- | Проверить закончина ли игра и если да то отобразить сообщение об этом
checkFinishGameProcess :: String -> NGame -> NGame
checkFinishGameProcess _ g = if checkFinishGame (nField g)
    then g{nFinish=True} else g

-- | Читать время в nCTime
getCurrentTimeIn :: String -> NGame -> IO NGame
getCurrentTimeIn _ g = do
    t <- getCurrentTime
    return g{nCTime = t}

-- | Процесс удаления устаревших анимаций
-- Но перед удаление выполняет фукцию записанную в анимации
deleteOldAnimation :: String -> NGame -> NGame
deleteOldAnimation _ g = deleteOldAnimation' g (nCTime g) 0
    -- g{nAnimation = deleteOldAnimation' (nAnimation g) (nCTime g)}
    where
        deleteOldAnimation' :: NGame -> UTCTime -> Int -> NGame
        deleteOldAnimation' g t n = 
            let a = nAnimation g !! n in
            if length (nAnimation g) <= n then g
                else if diff' t (getBeginTimeFromAnimation a) > getFinishTimeFromAnimation a
                    then deleteOldAnimation' (delA' (getFunctionFromAnimation a g) n) t n
                    -- ^ Выполнить анимацию из функции и удалить эту анимацию
                    else deleteOldAnimation' g t (n+1)

        delA' :: NGame -> Int -> NGame
        delA' g n = let a = nAnimation g
                        f = remove' n a in
                    g{nAnimation = f}

        remove' x list = if null list then list else take x list ++ drop (x+1) list

        diff' t x = realToFrac $ diffUTCTime t x

-- | Начало игры
initProcessesIO = [
      ProcessIO "createNumberTwo" createNumberTwo
    , ProcessIO "getCurrentTimeIn" getCurrentTimeIn
    ]

-- | Начало игры
initProcesses = [
      Process "checkFinishGameProcess" checkFinishGameProcess
    , Process "deleteOldAnimation" deleteOldAnimation
    ]

----------------------------------------------------

-- | Создать анимацию появления нового числа на поле
animationNew :: UTCTime -> Int -> Int -> Int -> Animation
animationNew time value x y = New time 0.15 value (x,y) $ f (NewCell x y value)
    where
        f :: NewCell -> NGame -> NGame
        f new g = g{nField = applyNewCellOnField new (nField g)}
        -- ^ Функция создает на поле новую ячейку после выполнения анимации

-- | Создать анимацию перемещения
animationMove :: UTCTime -> [Command] -> Animation
animationMove time commands = Move time 0.15 commands (f commands)
    where
        f commands' g = let t = g{nField = applyCommands commands' (nField g)} in
            addProcessIO t (ProcessIO "createNumber" createNumber)

-- | Проверить выполнение анимации
-- Во время анимации нельзя сделать ход. True если есть анимация, иначе False
checkAnimation :: NGame -> Bool
checkAnimation g = not $ null (nAnimation g)

getBeginTimeFromAnimation :: Animation -> UTCTime
getBeginTimeFromAnimation  (New  t _ _ _ _) = t
getBeginTimeFromAnimation  (Move t _ _ _) = t

getFinishTimeFromAnimation :: Animation -> Double
getFinishTimeFromAnimation (New  _ f _ _ _) = f
getFinishTimeFromAnimation (Move _ f _ _) = f -- TODO

getFunctionFromAnimation :: Animation -> (NGame -> NGame)
getFunctionFromAnimation   (New  _ _ _ _ f) = f
getFunctionFromAnimation   (Move _ _ _ f) = f
