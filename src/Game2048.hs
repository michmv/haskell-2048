{-# LANGUAGE MultiWayIf #-}
module Game2048 where

import Data.List (transpose, )
import Data.Sequence (fromList, update)
import Data.Foldable (toList)
import System.Random (randomRIO)

-- | Всегда должно быть [[Int, Int, Int, Int], [Int, Int, Int, Int], [Int, Int, Int, Int], [Int, Int, Int, Int]].
newtype Field = Field [[Int]] deriving Show

instance Eq Field where
    (==) (Field x1) (Field x2) = x1 == x2
    (/=) (Field x1) (Field x2) = not (x1 == x2)

data Move = MUp | MRight | MDown | MLeft deriving Show

data Rotate = RLeft | RRight deriving Show

-- | Координаты, отсчет идет от верхнего левого угла и начитается с нуля. Первая это горизонталь, второе вертикаль.
data P = P Int Int deriving Show

instance Eq P where
    (==) (P x1 y1) (P x2 y2) = x1 == x2 && y1 == y2
    (/=) x1 x2 = not (x1 == x2)

-- | Последний флаг показывает производить ли сложение или нет. Последнее значение это число над которым происходит операция.
data Command = Command P P Int deriving Show

instance Eq Command where
    (==) (Command x1 y1 n1) (Command x2 y2 n2) = x1 == x2 && y1 == y2 && n1 == n2
    (/=) x1 x2 = not (x1 == x2)

initField = Field [
      [0, 0, 0, 0]
    , [0, 0, 0, 0]
    , [0, 0, 0, 0]
    , [0, 0, 0, 0]
    ]

-- | Новое число
data NewCell = NewCell Int -- X
                       Int -- Y
                       Int -- Value

-- | Создать случайную ячейку
createRandomNumber :: Field -> IO NewCell
createRandomNumber (Field x) = do
    field <- createRandomNumber' x 0
    return field
    where
        createRandomNumber' :: [[Int]] -> Int -> IO NewCell
        createRandomNumber' field n = if n < 5000
            then do -- попытаться отыскать свободную ячейку и создать число
                x <- randomRIO (0::Int, 3::Int)
                y <- randomRIO (0::Int, 3::Int)
                if get field (P x y) == 0
                    then do
                        new <- createNewNumber
                        return (NewCell x y new)
                    else createRandomNumber' field $! (n+1)
            else do -- записать число в первую попавшуюся ячейку
                new <- createNewNumber
                position <- return $ strongSearch field 0 0
                return (NewCell (fst position) (snd position) new)

        createNewNumber :: IO Int
        createNewNumber = do
            randomValue <- randomRIO(0::Float, 100::Float)
            return $ callRandomValue randomValue

        callRandomValue :: Float -> Int
        callRandomValue x = if x < 90.9090909 then 2 else 4

        strongSearch :: [[Int]] -> Int -> Int -> (Int, Int)
        strongSearch field x y =
            let newPostionX x = if x == 3 then 0 else x+1
                newPostionY x y= if x == 3 then y+1 else y
            in
            if x > 3 || y > 3 then error "Can't create new cell"
                else if get field (P x y) == 0 then (x, y)
                    else strongSearch field (newPostionX x) (newPostionY x y)

-- | Выставить число на поле
applyNewCellOnField :: NewCell -> Field -> Field
applyNewCellOnField (NewCell x y n) (Field field) = Field (set field (P x y) n)

-- | Создать следующий шаг.
createCommands :: Field -> Move -> [Command]
createCommands fiels move =
    case move of
         MUp    -> rotateCommands (moveLeft (rotateField fiels RLeft)) RRight
         MRight -> rotateCommands (rotateCommands (moveLeft (rotateField (rotateField fiels RRight) RRight)) RRight) RRight
         MDown  -> rotateCommands (moveLeft (rotateField fiels RRight)) RLeft
         MLeft  -> moveLeft fiels

-- | Применить шаг к полю.
applyCommands :: [Command] -> Field -> Field
applyCommands commands (Field x) = Field (applyCommands' commands x)
    where
        applyCommands' :: [Command] -> [[Int]] -> [[Int]]
        applyCommands' [] fiels = fiels
        applyCommands' (Command source target newValue:cs) field = applyCommands' cs (set fieldUpdate source 0)
            where
                fieldUpdate = set field target (newValue + oldValue)
                oldValue = get field target


-- | Игра закончится когда на поле не останется нулей и не будет рядом стоящих чисел
checkFinishGame :: Field -> Bool
checkFinishGame field@(Field x) = not $ findZero x || findPair x || findPair (getMatrix (rotateField field RRight))
    where
        getMatrix (Field x) = x

        findZero [] = False
        findZero (x:xs) = findZero' x || findZero xs

        findZero' [] = False
        findZero' (x:xs) = x == 0 || findZero' xs

        findPair [] = False
        findPair (x:xs) = findPair' x 0 || findPair xs

        findPair' :: [Int] -> Int -> Bool
        findPair' [] _ = False
        findPair' (x:xs) n = (x == n) || findPair' xs x

-- | Найти максимальное число на поле
readMaximumScore :: Field -> Int
readMaximumScore (Field list) = findMax 0 (map (findMax 0) list)
    where
        findMax n [] = n
        findMax n (x:xs) = if n > x then findMax n xs else findMax x xs

-- | Повернуть поле на 90 градусов.
rotateField :: Field -> Rotate -> Field
rotateField (Field matrix) directionTurn =
    case directionTurn of
        RRight -> Field (transpose . reverse $ matrix)
        RLeft  -> Field (reverse . transpose $ matrix)

-- | Повернуть команды на 90 градусов.
rotateCommands :: [Command] -> Rotate -> [Command]
rotateCommands [] _ = []
rotateCommands (Command source target n:xs) directionTurn = 
    let r x = rotatePoint x directionTurn in
    Command (r source) (r target) n : rotateCommands xs directionTurn

----------------------------------------------------------------------------------

rotatePoint :: P -> Rotate -> P
rotatePoint point directionTurn =
    let toVector (P x y) = P (2*x-3) (2*y-3)
        rotateRight (P x y) = P (-1*y) x
        rotateLeft (P x y) = P y (-1*x)
        fromVector (P x y) = P ((x+3) `div` 2) ((y+3) `div` 2)
    in
    case directionTurn of RRight -> fromVector . rotateRight . toVector $ point
                          RLeft  -> fromVector . rotateLeft  . toVector $ point

-- | Создать список команд для смещения элементов налево
moveLeft :: Field -> [Command]
moveLeft (Field []) = error "Field is not correct list"
moveLeft (Field x) = moveLeftList2 x 0

moveLeftList2 :: [[Int]] -> Int -> [Command]
moveLeftList2 [] _ = []
moveLeftList2 (n:ns) y = moveLeftRow n y ++ moveLeftList2 ns (y+1)

moveLeftRow :: [Int] -> Int -> [Command]
moveLeftRow [] _ = []
moveLeftRow list y = if length list == 4 then ttt (tail list) 0 (head list) 0 y else error "List must have 4 elements"
--moveLeftRow list y = if length list == 4 then moveLeftRow_ (tail list) 1 y (0, head list) else error "List must have 4 elements"
    where
        -- | Передовать первым параметром хвост массива, а голову передовать через последний параметр. Смотри функцию moveLeftList2
        moveLeftRow_ :: [Int] -> Int -> Int -> (Int, Int) -> [Command]
        moveLeftRow_ [] _ _ _ = []
        moveLeftRow_ (currentValue:vs) x y cursor@(positionCursor, valueCursor) =
            --trace ("__currentValue=" ++ show currentValue ++ " x=" ++ show x ++ " y=" ++ show y ++ " positionCursor=" ++ show positionCursor ++ " valueCursor=" ++ show valueCursor ++ "__") $
            if | currentValue == valueCursor && currentValue /= 0 && x /= positionCursor -> Command (P x y) (P positionCursor y) currentValue : moveLeftRow_ vs (x+1) y (positionCursor+1, 0)
               -- ^ если нужно слияние то слить и сместить курстор

               | currentValue /= 0 && valueCursor == 0 && x /= positionCursor            ->  Command (P x y) (P positionCursor y) currentValue : moveLeftRow_ vs (x+1) y (positionCursor, currentValue)
               -- ^ если нужно переместить то переместить, курстор остается на своем месте

               | currentValue /= 0 && valueCursor /= 0 && x /= positionCursor            -> moveLeftRow_ (currentValue:vs) x y (positionCursor+1, 0)
               -- ^ если текущее число и число в курсоре не совместить то сдивинуть курсор

               | otherwise                                        -> moveLeftRow_ vs (x+1) y cursor
               -- ^ взять следующее число

        ttt :: [Int] -> Int -> Int -> Int -> Int -> [Command]
        ttt inList pIn cursor posCursor y = if pIn > 2 then []
            else --trace ("__x="++show x++" cursor="++show cursor++" posCursor="++show posCursor++"__") $
                if | x /= 0 && cursor /= 0 && x /= cursor  -> if pIn /= posCursor then Command (P (pIn+1) y) (P (posCursor+1) y) x : ttt inList (pIn+1) x (posCursor+1) y
                                                                                  else ttt inList (pIn+1) x (posCursor+1) y
                    -- ^ m n записать за позицией курсора и сметить оба указателя, курсок указывает на записанное чило
                   | x == cursor && x /= 0 -> Command (P (pIn+1) y) (P posCursor y) x : ttt inList (pIn+1) 0 (posCursor+1) y
                    -- ^ n n слить два числа и сместить оба указателя, курсор указывает на ноль
                   | x /= 0 && cursor == 0 -> Command (P (pIn+1) y) (P posCursor y) x : ttt inList (pIn+1) x posCursor y
                    -- ^ 0 n записать число в курсор, сдвинуть только указатель на число
                   | x == 0 && cursor /= 0 -> ttt inList (pIn+1) cursor posCursor y
                    -- ^ n 0 сдвинуть указатель на число
                   | otherwise -> ttt inList (pIn+1) 0 posCursor y
                    -- ^ 0 0 сдвинуть указатель на число

            where x = inList !! pIn

set :: [[Int]] -> P -> Int -> [[Int]]
set field (P x y) new = toList $ update y newRow (fromList field)
    where newRow = toList (update x new (fromList (field !! y)))

get :: [[Int]] -> P -> Int
get field (P x y) = (field !! y) !! x
