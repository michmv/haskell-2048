module Types where

import Basegame (Action(..), ActionBind(..), ProcessIO(..), Process(..), GameClass(..))
import Data.Time (UTCTime)

import Game2048

data NGame = NGame
    { nActions         :: [Action NGame]
    , nActionsBind     :: [ActionBind]
    , nActionOtherwise :: Maybe (String -> NGame -> NGame)
    , nStop            :: Bool -- флаг остановки приложения
    , nProcessesIO     :: [ProcessIO NGame]
    , nProcesses       :: [Process NGame]
    , nWindow          :: (Int, Int)
    --
    , nCTime           :: UTCTime
    , nField           :: Field
    , nHelp            :: Bool -- флаг показать помощь
    , nFinish          :: Bool -- игра закончена
    , nAnimation       :: [Animation]
    , nLang            :: Lang
    }

instance GameClass NGame where
    getActions             = nActions
    getActionsBind         = nActionsBind
    getActionOtherwise     = nActionOtherwise
    getStop                = nStop
    getProcesses           = nProcesses
    getProcessesIO         = nProcessesIO
    getWindow              = nWindow

    setActions p g         = g{nActions=p}
    setActionsBind p g     = g{nActionsBind=p}
    setActionOtherwise p g = g{nActionOtherwise=p}
    setStop p g            = g{nStop=p}
    setProcesses p g       = g{nProcesses=p}
    setProcessesIO p g     = g{nProcessesIO=p}
    setWindow p g          = g{nWindow=p}

data Animation = New  UTCTime    -- время начало анимации
                      Double     -- продолжительность в секундах
                      Int        -- значение в новом поле
                      (Int, Int) -- координаты X и Y, от 0 до 3
                      (NGame -> NGame) -- функция которая будет выполнена после завершения анимиции
               | Move UTCTime   -- время начала анимации
                      Double    -- продолжительность
                      [Command] -- список команд для изменений
                      (NGame -> NGame) -- функция которая будет выполнена после завершения анимиции

data Lang = RU | EN
